import java.util.Scanner;

public class GradeBook
{
	private String courseName;
	private Scanner input;
	
	public GradeBook(String name)
	{
		courseName = name;
	}

	public String getCourseName()
	{
		return courseName;
	}

	public void setCourseName(String courseName)
	{
		this.courseName = courseName;
	}
	
	public void displayMessage()
	{
		System.out.printf("Welcome to the grade book for %s\n", getCourseName());
	}
	
	public void classAverage()
	{
		input = new Scanner(System.in);
		
		int grade;
		int total = 0;
		int avg;
		int count;
		int i = 0;
		
		System.out.println("Please enter the number of grades to average:");
		count = input.nextInt();
		
		while (i<count)
		{
			System.out.println("Please enter a grade:");
			grade = input.nextInt();
			total = total + grade;
			i++;
		}
		
		avg = total/count;
		
		System.out.printf("The total of all %s grades is %s.\n", count, total);
		System.out.printf("The average grade of the class is %s.", avg);
		
	}
	
	
}
